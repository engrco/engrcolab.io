## Knowledge Engineering

***...is about accelerating the efficient progression of curiosity*** ... and that is about improving the conversion of data into information, improving the conversion of information into knowledge, improving the conversion of the complexities of knowledge into ordered wisdom and improving the utilization of acquired wisdom for informed, actionable, creative insight and expression.

There is no room for outright censorship [of something that offends us] in the conversion process of legitimate knowledge engineering ... for example, even garbage data informs our the improvement of data collection processes just as misinformation and propaganda betray weakness. ***We don't waste noise; we find uses for it.***
### Reading

Reading is like weightlifting. We read more in order to read better, to read faster, to read even more, to level up our skills of adding machine intelligence to our overall intelligence gathering processes.
#### Annotated, Curated Lists

We use social bookmarking and social bibliography tools in order to connect with readers, annotaters, curators.
#### RSS Feeds and Readers / Pre-Print Archives

We use social RSS feed readers like NewsBlur in order make the peer-review process more transparent.
#### Connections / Knowledge Graphs

### Data Collaboration

#### APIs / Pipelines / GitOps

#### Preparation / Pre-Processing

#### Visualization / Graphs


#### Analysis / Ontology 