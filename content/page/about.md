---
title: About ENGR.co
subtitle: ENGRco exists to LISTEN
comments: false
---

## We ENGINEER better listening discipline

Discipline is the key to [fighting through crap to achieve] independence. **#DISCIPLINEequalsFREEDOM** 

Listening discipline deliver intuition. You don't just get intuition; it must develop from practice, good habits, continually improving workflows and extra efforts to sustain and improve the tenacity to listen, intuit and observe more effectively.

EFFECTIVE observation is the opposite of *data dumps* or *information overload* -- it's about understanding oceans of data and being able to pull out exactly the slice of data to focus on to give information that is needed ... this level of skill can be coached, but it's really about curiosity and needing to know WHY rather than memorizing a recipe or set of facts.  

We can help coach you ... to better implement, better build, better extend and better *use* available open source [cloud native](https://github.com/cncf/toc/blob/main/DEFINITION.md) technologies ... but we are never going to be the kind of heros who come in and just do things for you.  You will bail yourself out of your own jams ... maybe with help ... we can coach you, but *you will not be allowed to need us*. 

Discipline is recognizing that one's discipline is never perfect, never sufficient ... this is why independence and freedom require making the commitment to sustain and improve your discipline is up to you and you must do that in a way that makes sense for you, for your people, for your organization.  

### We USE our DISCIPLINE to LISTEN

We are hawks on discipline, but one cultural approach does not fit all ... we can all get along, but we are not all one big happy family ... our cultural approach fits us; you will and must develop your approach.  At some point, you must OWN your own culture ... you cannot be a disciple forever; you must ascend to mastery of your own discipline. 

Our internal discipline is about **engineered continuous improvement** ... so when we say that we [*dogfood* our engineering discipline](https://engrco.gitlab.io/page/process/), it means that we are continually gather intell, continually adjust, revise, edit, simplify, re-invent and sample and enjoy and live on our own discipline. It will never be perfect; we will always be perfecting it. 

*Dogfooding means that it will have to work for us ... or we die.* Obviously, we must embody *engineered continuous improvement* first ... and before we write about it ... before we coach it. 

We use to separate work from life ... no longer ... our intention is to improve to become healthier, more fit, generally much better informed ... to improve so that we are perhaps even unrecognizable to some in five years. We are minimalists -- we reduce our costs and work toward doubling important capacities by a factor of 2 each year. 

We don't want to just fade, disappear and die -- we exist to fight in REAL battles, REAL wars ... there's no reason for living in some fantasyworld of the wars or battles of the last decade; there's even lesst reason to live in the rear view mirror.  We must embody *engineered continuous improvement* first.  It will have to work for us ... or we die ... we intend to die fighting.
 
### No Filter, No Prisoners, No Heroes
 
We are a team, building the discipline of teamwork and listening. 
 
There's nothing wrong with companies. We just are not one. Companies [must] become somewhat inflexible and fixed; they become targets because of how the company structure is a financial tool to collect money and then invest that money in a manner that hopefully perpetuates the company while providing employment and security to employees of the company.  
 
All companies ultimately fail. While a few can last longer than humans, that is actually incredibly rare. Most companies fail within the first few years of existence ... it takes an incredible productive team in order to have a chance at having a company that survives ... our agenda is to put our customers and our team first -- we are not a company.  
 
We are TEAM ... we care about engineering better listening discipline ... we are not a company; we are not worried about being around for decades.  We absolutely do wish those who do intend to build something lasting the best of luck. 
 
We are, before anything else, a TEAM. We are not a company.  We do not need or want any captives ...  we refuse enter into long-term contracts or take prisoners ... none of our investment is dedicated to monetizing our network of customers or clients. We look for customers or clients who we can help as colleagues, as potential team members.
 
### No Risk, No Expectations, No Ego
 
The best way to start out with us is probably with the FREE general content which we offer to anyone ... that should set your expectations low enough ... we are pretty humble people; we have a lot to be humle about.
 
If our free content is not annoying enough for you, we can really get under your skin monthly subscription plans for professional engineering services specific to your own situation.  

We refuse to accept business unless we know that we can reduce costs, improve your productivity or general level of health and provide you with financial results that more than offset our cost.  

We welcome your proposals ... we might suggest thinking about something like a one month trial *date* ... which might involve something like 20 hrs of coding/implementation/advisory services ... if that's still not annoying enough for you, we can try harder.