---
title: Data Engineering Drives Vision
subtitle: Ingest, Store, Process, Analyze, Explore, Visualize and MAINTAIN 
comments: false
---

## We do SIMPLICITY engineering 

The WHY of our collaborative open source engineering is about truly ***asynchronous workflows*** (ie the kind of projects that people put *on the back burner* for a while and come back to after five years and stuff still works) ... we are impressed by maintainabilty and extensibility as well as tight scopes with focus for composability

The goal of simplicity engineering is achieving a state of anonymity -- where simple stuff is taken for granted because it just works ... we cannot understand the smaller why of why someone might want to compose something for performance of a specific task. In other words, our aim is to be the janitor who cleans and 5Ss the area when people are not around and maybe maintains or rebuilds stuff.

## SIMPLICITY is all about understanding what the point is ... rather than just doing something

Understand the larger WHY of what you are doing as well as the smaller whys of what you must do to get you to a more efficient way of doing what you will do.  This applies to data engineering; for example, we don't just ingest and store data because the data exists -- processing and analysis off that data are going to need to yield actionable, valuable insights ... we are going to need to do exploratory plotting and visualization, at first in order to double check the soundness of our data upon which our insights are based, but later to explore ways to refine the process and ensure that we continue to improve.

Data engineering is all about optimizing the actionable value of gathered data -- of course, costs are involved in ingesting, storing, processing data and producing visualizations, so work very hard continually understanding better and better and better ways to do this continual optimization. The point of it all involves a better product or service that more efficiently greater value.

Focusing upon understanding the WHY of work makes it future-driven and tends to drive out those path dependendent things that people do because that's they way they have done it ... engineering dataflow and improving development workflows requires siezing opportunities for improvment and accelerating the dataflow for developing better workflows ... data engineering is largely dogfooding, ie we are always developing, immproving, simplifying something that will help us develop something better tomorrow.

If you want to accelerate development of improvement ... it is probably going to require the PARTICIPATION in, or at least the awareness of, making open source software COMMUNITIES better. Enable pushing hard in [a non-linear workday](https://about.gitlab.com/company/culture/all-remote/non-linear-workday/) ... AT LEAST 50% of the creative process requires you to not be working, it might push to 80% or 90% or 95% as you do more conceptual or exploratory work. 

Siezing opportunities means driving development asynchronously with [Issues and Issue Boards](https://docs.gitlab.com/ee/user/project/issues/#issues) to improve the collaboration on feature proposals and support requests, fully understand all dimensions of a problem, find root cause and to solve problems, and then prepose/plan workflow and track status/implementation.

An agile development workflow requires moving toward small feature releases continually with multiple releases in any given day. Test-driven development and automated checks in the build process must allow to developers *shift further left* to release much smaller feature improvements and debug the unknown unknowns that arise from different customer experiences in by observing and correcting running Production systems. 