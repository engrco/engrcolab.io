---
title: Baking in the Monitoring
subtitle: SRE and Database Reliability Engineering
comments: false
---


# You don't get another NOW

We don't monitor for the sake of monitoring ... we monitor for the sake of being able to get another look at an important moment -- to focus more of our attention on the nature of where a problem happened ... our cursiosity drives our need to inform the depth of our understanding of why things fail. When we don't aggressively get rid of excessive monitoring, we are consuming **the most important, most scarce resource that humans have ... our attention span.**  It cannot be emphasized enough ... MOST, almost all, forms of information and marketing hype are either poisonous narrative-driven programming or just temporary shiny flashes in the pan ... one needs to look at a lot of things and then be IMPATIENT, IMPOSSIBLE to please asshole ... because **the most important, most scarce resource that humans have ... our attention span.**

The lesson that we see throughout history is that most manmade catastrophes are the result of intelligence failures ... there are times when it's a lack of courage, but most often it's a lack of paying attention to the things that we should've known we should be paying more attention to.  Not paying attention KILLS ... so we gather intelligence and improve our intelligence gathering efforts. Professional listening means curating [knowledge graphs](https://en.wikipedia.org/wiki/Knowledge_graph) and developing new networks and relationships for better [humint](https://en.wikipedia.org/wiki/Human_intelligence_(intelligence_gathering)), [sigint](https://en.wikipedia.org/wiki/Signals_intelligence), [imint](https://en.wikipedia.org/wiki/Imagery_intelligence) or [masint](https://en.wikipedia.org/wiki/Measurement_and_signature_intelligence).






