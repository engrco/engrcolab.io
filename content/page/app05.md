---
title: Developing the discipline of applied intuition and curiosity
subtitle: What Is Observability Engineering For?
comments: false
---

***WARNING: The first RULE of every chapter is to first look around and then critically review/revise or even completely throw out the following background for a this chapter ... FIRST ... LOOK ... AROUND ... somebody might already have a better solution. Steal that idea and build on it.***

Speed is power ... FOCUS your acceleration of speed upon those things which you do OFTEN ... REMOVE all uncessary steps ... and then aggressively automate the necessary steps with something akin to a [CI/CD build pipeline](https://docs.gitlab.com/ee/ci/pipelines/). When we say that, "speed is power" ... we mean that THINKING about value first and then THINKING about automation gives you POWER ... as with health, it's not what you add or swallow to *supercharge your engine* ... SPEED is about what you REMOVE, what you take away and then making good habits either involuntary and just part of what you do without thinking OR automating tasks for flawless execution and removal of errors ... SPEED is about what you REMOVE ... REMOVE the waste, remove the errors.

There's no way to overstate the importances of AGGRESSIVELY minimizing and removing the unnecessary ... 