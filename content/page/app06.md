---
title: Design, dashboards, data visualization
subtitle: Really communicating interactively with data
comments: false
---

***WARNING: The first RULE of every chapter is to first look around and then critically review/revise or even completely throw out the following background for a this chapter ... FIRST ... LOOK ... AROUND ... somebody might already have a better solution. Steal that idea and build on it.***

Asynch is EATING the world ... as it shortens up the development cycles by removing meetings and the dysfunctional cultures that rely upon meetings and work-shaming ... pushing knowledge cultures to more efficient, efficacious dev tools and [constantly improving build pipelines](https://docs.gitlab.com/ee/ci/pipelines/) ... using up the whole clock to hand-off dev from one asynch dev to another.

Asynch is POST-national ... POST-global ... POST-industrial ... meetings and Zoom webinars should be fewer and farther between ... most of the communication should be done by someone who can explore the data on their own ... asynch.work is about *LEAN*ing the F up about everything in a workflow ... that includes the whole notion of what a firm is or WHY a firm hires slaves or contractors to toil on its plantation, assembly lines or salt mines.

## Machines are machines. Humans are humans.  

Stop expecting humans to adapt to your machine design.  

When you grind up humans in industrial, meeting-heavy workflows, you should EXPECT burn-out ... and you shouldn't be surprised when people ***GO POSTAL*** ... or EXIT to be coddled elsewhere by someone who can use the damaged, folded, spindled, mutilated, industrial goods which they now are.

It's akin to, but NOT really like the Analyzulator (GYG02) but it's intended to look more at cultural engineering ... gathering ideas about REMOVING meetings while IMPROVING communication.  

It's not JUST about Git, of course -- it's about optimize your ENTIRE toolchain and ENTIRE workflow for INDEPENDENCE ... asynch.WORK can be Git-centric, eg using GitHub or Gitlab as social media to discuss various topics, eg debate optimizing approach with other knowledgable optimizers ... the objective is developing a knowledgenetwork for LEAN discussions are more productive, more like "sharpening the axe" in order to better, more efficiently, more efficaciously "chop the wood."

**For practical purposes, our implementation of asynch.WORK will rely heavily on everything about the Gitlab workflow.**