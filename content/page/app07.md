---
title: Data Wrangling
subtitle: Data APIS, think in GraphQL
comments: false
---

***WARNING: The first RULE of every chapter is to first look around and then critically review/revise or even completely throw out the following background for a this chapter ... FIRST ... LOOK ... AROUND ... somebody might already have a better solution. Steal that idea and build on it.***

This chapter is an [Agile Data Science 2.0](https://learning.oreilly.com/library/view/agile-data-science/9781491960103/) LEARNING exercise. AI/ML Ops depends upon data wrangling, consuming data APIs from a wide range of ***EXISTING*** sources, eg [Indeed](https://opensource.indeedeng.io/api-documentation/) ... while also aggregating, analyzing, and generating data, hopefully as GraphQL data APIs. 

There are going to be a lot of different approaches that have been and will continue to be used in structuring APIs ... there's not really an substitute for being something of a data polyglot or conversant in different structures, different nomenclatures that structure how different people think about [imperative, declarative, and workflow APIs](https://apievangelist.com/2019/07/16/imperative-declarative-and-workflow-apis/) ... when it comes to the data that you produce, you probably want to start getting yourself and others to move in the direction of primarily [thinking in graphs](https://graphql.org/learn/thinking-in-graphs/) because graphs are especially powerful tools for humans who think visually so modeling many real-world phenomena tends to generally resemble our natural mental models and verbal descriptions of underlying processes. 


