---
title: Security
subtitle: BEFORE THE START OF THE START
comments: false
---

***WARNING: The first RULE of every chapter is to first look around and then critically review/revise or even completely throw out the following background for a this chapter ... FIRST ... LOOK ... AROUND ... somebody might already have a better solution. Steal that idea and build on it.***

Information SECURITY must be designed in from BEFORE the start or, IDEALLY, it should happen BEFORE THE START OF THE START ... as an internalized design sensibility or awarness informing the choice of the architecture before the architecture can be developed, so early that it inspires the design before the design is designed ... long, long before anything else is built.

Think about the objective of something practical ... like accelerated negotiation app for a global exchange that allows for qualified bidders, buyers, sellers of items of known quality.  This sort of thing has to start way, way, way up front -- it simply can never involve attempts to *pretty up* or *put bandaids on* questionable participants or questionable things being bid on. It's like the Super Bowl ... ALMOST ALL of the GAME happened way before the first play.  It's not al all about the END result ... it's about setting it up so that the GAME has the chance to be the BEST GAME OF ALL TIME. It cannot be about a game were insiders or criminals who know about a hidden loophole in a rule and have an incentive to even try to get something for nothing.  Designing the accelerated negotiation is about understanding the motives of ALL potential players in the system.