---
title: Contributing to Open Source
subtitle: Learn how others USE what you USE
comments: false
---

***WARNING: The first RULE of every chapter is to first look around and then critically review/revise or even completely throw out the following background for a this chapter ... FIRST ... LOOK ... AROUND ... somebody might already have a better solution. Steal that idea and build on it.***

## Philosophy

***Contributing to open source is really about learning how other developers THINK through their development workflow ... helping people to THINK more clearly to build software and apps that JUST WORK intuitively, with fewer distractions is about about deep, fundamental, habitual, ubiquiotus, utilitarian KINDESS to others.***

Contributing to open source software communities, especially an [open software project like GitLab.ORG that which is about the development workflow itself](https://gitlab.com/groups/gitlab-org/-/issues) make it necessary to think outside of one's intellectual comfort zone. If one spends much of any time at all reading through discussions and getting a lay of the land od different topics and drilling down to the [Issue discussions](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=workflow%3A%3Aready+for+development), then one quickly sees that how many think different, probably, just generally think better albeit in a narrowly focused manner [which is actually necessary to get things done] -- mostly, it is about realizing that there are completely mindblowingly different ways to **think WELL**. 

**Contributing to open source software communities might not immediately lead to thinking well ... it immediately leads to thinking better.**

There are some general themes that tend to emerge: 

1) **Pretty much ALL people have different sorts of underlying family matters, health issues, pains, fears or sources of stress.**  ALL means ALL, even though some have more *on their plate* than others and not all people are so stoic. Some people make excuses; use their sources of pain or stress as an excuse to not show up, to not do things.  In the parlance of sports, however, "People who DO things play hurt."  This is akin to the fact that the very busiest people are the actually ones who most capable of taking on more [temporarily] ... even hero syndrome burns people out and can *poison the well* for the busy person who becomes too busy to pay attention to things like diet, health, fitness or sleep.  What tends to really scramble or confuse most thought processes even more than stress or pain, is that almost nobody fully realizes that pretty much ALL people they encounter while doing things is ALSO going to have some sort of underlying health issue, pain, fear or source of stress AND also not paying attention things like diet, health, fitness or sleep.  

2) **Those who really NEED to be around like-minded people face enormous obstacles -- there is something gigantically immature, gigantically wrong with the brittleness of their child-like need to be intellectually-coddled.**  When you encounter someone who needs to be around like-minded people, unless there is a NEED to correct that behavior, you can just leave them alone and safely avoid spending that much effort on your relationship with them because they are not going anywhere and they are not going to change until they decide to change. Contributing to open source is about meeting people who genuine respect how others think and can routinely switch how they think in order to help other people solve problems ... in other words, the people that you probably want to meet are those who do not really have a personality type, but instead can adapt their personality along with their thinking to best suit the situation at hand.

3) **Genuine kindness is about respecting how others think ... phatic conversation, joking, celebrating success, LISTENING, developing intuition, seeing the human need, understanding their current frustration ... and helping them to think more clearly where their train of thought is going.** The compounded background stressors that others feel can build, get in the way of clarity of thinking ... people can forget WHO is the customer, WHY are we doing this, WHAT is the objective ... lower level stressors that are not dealt with ultimately destroy team cohesiveness. Lack of awareness of the stress of others tends to gets in the way of thinking in terms of team collaboration, ie teams get stuck when team members become suspicious or jealous of colleagues INSTEAD of asking a question like, "I respect your fears of X and I am deeply sorry for being so thickheaded, but I don't think I understand the full scope and depth X ... could you walk me through that? I apologize for the question ... is it ___ ... I know that I must be missing something here."



# DISCIPLINE = FREEDOM 



## Recognize WASTE and the EXCUSE factory that feeds it
