---
title: Service Mesh
subtitle: What do people want to DO with this shit?
comments: false
---

***WARNING: The first RULE of every chapter is to first look around and then critically review/revise or even completely throw out the following background for a this chapter ... FIRST ... LOOK ... AROUND ... somebody might already have a better solution. Steal that idea and build on it.***

Edison said that innovation is 99% perspiration, 1% inspiration ... FUCK imagination, we can automate perspiration to remove most of the tedious -- for example, with ML ops we can now be much better at gathering intell ... to better answer quesetions like what do people NEED?  

But, don't worry ... invention will still be HARD and tedious -- ***the automation of tedious will expose new layers of tedious*** ... regardless of however easy AI/ML ops make things ... it is just like how memorization USED TO BE an indication of intelligence -- but now humans just google things ... we don't really have to worry about AI/ML ops making human creativity unnecessary ... regardless of whatever we get machines to do for us, there will always be new impossibly difficult, HARD, tedious challenges that require a ridiculous level of curiosity, humor, intuitive creativity and the effort to do things that make us uncomfortable ... ***don't worry, princess, discomfort will always be with us.***