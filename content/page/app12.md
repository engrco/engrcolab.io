---
title: WHY containers and K8s matter
subtitle: Setting it up so that everyone in the world can get paid for what they are phenomenal at
comments: false
---

***WARNING: The first RULE of every chapter is to first look around and then critically review/revise or even completely throw out the following background for a this chapter ... FIRST ... LOOK ... AROUND ... somebody might already have a better solution. Steal that idea and build on it.***

This particular chapter is kind of a culmination of the other chapters ... WHY WHY WHY WHY WHY ... and lots of other questions like how big is the need, who pays, why now or what has changed in terms of timing and enabling technologies/external shocks ... the *fad* of containers is not important -- what IS important is about REALLY *KNOWING* the WHY of why you can't live without containers ... you can safely avoid crowds and always should be skeptical of herds flocking to something that is widely hyped, but **sometimes the WHY is indeed really important** ... it's not just hype or peer-pressure ... it's a legit WHY.

## The most important thing is know your WHY

Maybe it's ***WWJD*** ... that'd be excellent, but if Jesus Christ is not your role model for SUCCESS and DISCIPLINE ... fine, what would Jocko Willink or David Coggins suggest?  What would Socrates suggest?  What would Abraham Lincoln suggest?  What would Bob Dylan suggest. The success of others might be a guideline, any legit success knows that he not only built his success, but His Creator also gave him the insight to build the ONLY yardstick against which his success is to be measured ... there are two commandments LOVE your Creator with your heart and soul more than you love your life AND respect your life in the same way that you respect the lives of others, ie you do someone no favors when you pity them and say, "Aw ... you pathetic mess ... let me do that for you."

If you want to CREATE, you must LOVE. There is no substitute for LOVING life ... this is a big part of the reason why humans can make noise contraptions that burn resources and produce nothing but noise, pollution and other negatives ... the essence of Life is beautiful elegance, the awesomeness of things not yet fully understood. If you want to LEGITIMATELY create anything of value ... you must LOVE the beginning essence of Life ... and, at a minimum, this means that you must stop worshiping your own great work ... RESPECT is not a matter or fawning adoration ... RESPECT is about understanding why humility, the virtue that Ben Franklin refused to master, is the greatest virtue of any human ... accordingly, humility and it's necessary mate, the quest for anonymity [RATHER than fame, power and wealth] are truly RARE and thus, innately POWERFUL.

# We all learn the HARD way and, pretty much, only the HARD way.

A ***successful*** man gets up everyday and works HARD at exactly what he wants to work at all day long ... and then, without needing any distractions, delusions or drugs to help him cope with the ABSOLUTELY PATHETIC EXISTENCE that other less-than-successful people need, a ***successful*** man goes to bed and almost sleeps, but dreams of the success of getting up tomorrow and working HARD at exactly what he wants to work at ALL DAY LONG.

Rust never sleeps ... but you can learn something by looking at what it attacks successfully.  Consider, if you will, what if GoodReads readers converted their comments and perceptions of a book into a logotherapist who was the summation of all GoodReads comments?  Of course, this isn't JUST about GoodReads comments ... it's really about taking SERIOUS, THOUGHTFUL, CRITICAL comments from EVERYWHERE and turning those comments into something like a logotherapist.

So ease yourself down into that ice bath, breathe deeply and pretend Wim Hof was right there with you to guide through the experience, so that you get as much COLD as you can get out of that COLD.

## Avoid ALL addicts, especially the respectable addicts addicted to prescription pills or alcohol or daily news programming

When you make the choice to swallow benzos to deal with stress ... people will still have compassion for your suffering but they are not ever going to listen again ... if anyone [using prescription medication or alcohol or daily news programming] ever attempts to lecture you on any aspect of life, well ... we just don't EVER listen to drunks or prescription addicts or news junkies while they are still using, except maybe to *just* listen, as a professional listens to a patient.  Addicts don't get to dispense advice.

## Everything you have ever wanted is on the other side of some SMALL, but practical little fear that keeps you from daily work on your discipline

SMALL things are enormously important ... because of how they tend to *build* and ***Build*** and really **BUILD**.  

For example, consider the fear of a daily cold shower ... of course, the discipline of EVERY DAY, just crossing that line to jump into the cold water is a big deal -- jumping in ice water ONCE is just for idiots who claim, "Yeah, I did that!" ... but, the daily discipline of jumping into a cold shower is an important discipline to build because of how it translates into other things.  It's seems insignificant, but it is UNDENIABLY practical.  

It is important to work on the mindset that overcomes small PRACTICAL fears ... because those fears are holding you back, eg cold showers will help you accelerate your body's recovery processes from training; thus, the cold shower fear snowballs into going easy in your training regimen, because you can't recover as fast, so you need larger intervals between "leg day" when every maybe every single day should be "leg day." 