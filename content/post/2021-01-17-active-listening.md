---
title: The foundation of ENGRco is LISTENING
subtitle: Building a solid listening pyramid on a solid foundation
date: 2021-01-17
tags: ["listening"]
---

# We exist in order to engineer better listening

BEFORE we construct the LISTENING pyramid of skills, we must lay the foundation beneath the foundation beneath the foundation ... it's our WHY, our reason for existing.

That's why the most necessary bedrock foundations to begin to enable the zeroth listening skill is to COMPLETELY IGNORE, to STOP LISTENING to everything that might be noise, ie EVERYTHING.

As with most other forms of improvement, most of the gains come from REMOVING the impediments, and that includes automation, pain relievers and medicines ... most of improvement is in knowing what to take away, but generally it's better to err on the side of removal ... do not just remove ALL of the noise, remove all of the clever automation that is amping up the noise into something that seems to be important because it's loud fuzz.

When you LISTEN carefully to ideas, you will find that MOST of what you have been listening to is just DEAD manipulation, not actually an idea.

***The foundation rests upon the BASE that involves ignoring most noise and distractions that are thrust in our face. We make no apologies for IGNORING anyone who is obviously distracted with brain poison from news organizations.*** 

# Build Solid Foundations

For most of us, building something new involves following a well-established set of building practices RATHER than designing our own framework [until at least we have the experience and, more importantly, the burning desire or need to create a new architecture]. When it comes to building something in the virtual realm, we are big fans of the entire GitLab framework ... including the meta-framework which involves the open source [contributions to the Gitlab framework](https://about.gitlab.com/community/contribute/) ... in order to learn how to learn how to learn, we are going to NEED a Git-based distributed version control framework like that of Gitlab.

We must always remember that our goal is much better LISTENING to human beings ... facilitated by improved sitational awareness [in a global sense] and much better powers of data-driven observation ... which in turn is facilitated by generally better data science and machine learning operations skills; those skills do not exist just to be an empty structure of skills. 

***The goal of our skill building is BETTER listening.***  

By *better* we mean much more informed, much more aware and much more capable [because we actually DONE something and know how it should be done] ... Thus, **BETTER** listening is matter of habits and stone-cold continuously improvement DISCIPLINE.  The basic dev ops foundational skills and, specifically our use of the Gitlab continuous integration and continuous deployment framework is matter of daily DISCIPLINE -- that means small, daily commits in support of continuous habitual improvement of data science and machine learning operations skills.  

Hopefully, it's obviously that we must focus on our ultimate goal as we learn the [features of something, even something that is rapidly evolving like Gitlab](https://about.gitlab.com/features/). Achieving the goal of listening really depends upon our knowledge becoming almost instinctual, like a native language, going beyond mere factoids related to features.  We will always need to learn new features or new changes to features in order better express ideas and think more concretely, precisely in terms of using Gitlab vocabulary. 

In all parts of the foundation, architecture and structue ... we don't use the vocabulary for vocabulary's sake ... it's the same with the goal of improving our own ability to execute data science and ML operations, to achieve much better powers of data-driven observation, to have a stronger situation awareness to be better able to LISTEN ... while continously ignoring noise and distractions.

# Explore and Execute Attitude-Skills-Knowledge

In this case

# Develop Impactful Habits

Habits are the kinds of things that you do without thinking -- we all have habits, if we do not think deeply about impacts and then work at developing impactful habits [when we just would rather not work on our *developing* any more], then our habits will be lazy and we will always be the same guy that we didn't like before we decided to develop better habits ... the most essential habit is to never stop developing more impactful habits.

# TAKE Primary Responsibility

# Find and Align Purpose

# Apply SEIR (Success in Everyday Relationships)

# Identify and Control Distractions

# Identify and Use Structure

# Identify and Control Emotions

# Take Meaningful Actions

So what if you LISTEN, but don't RESPOND.  

Hopefully, educations systems have stopped training kids to just show up or just answer test questions well enough to be disgustingly mediocre, ie graduate with flying colors.

There ARE educational alternatives that are about training competence RATHER than baby-sitting for 20 years.

[Gradiance](https://www.gradiance.com/pub/inst-guide.html)'s goal is to offer affordable education that helps students legitimately MASTER both concepts and programming techniques, offering problem sets and giving advice **in response to errors.**   It's ALL about RESPONDING to the "not quite good enough yet" comments ... it's an automated approach in which the faculty and teaching assistants [LISTEN to students and industry to] develop more challenging work for students and/or spend more time with students who need help.  It turns on root questions which have students solve long-answer questions in a format that can be easily monitored and automated. Gradiance materials include explanations that help students whenever they miss a component of an assignment. Thus, the core principle of the root question format in Gradiance homeworks and labs is that the student is encouraged/forced to learn from mistakes and, eventually respond to ALL inadequacies, to get the assignment completely correct. Obviously, employers would prefer to have students who can actually FINISH an assignment CORRECTLY than just hiring students with diplomas, but it is interesting that students confirm that they found this approach to be a better experience than conventional assignments, ie an A on a test or paper is nice, but real-world confidence in being able to do the job feels a lot better.

