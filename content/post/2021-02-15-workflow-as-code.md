---
title: Remote Workflow-As-Code
date: 2021-02-15
tags: ["TABATA", "workflow-as-code", "Pomodoro" , "remote work"]
---



### Remote workers need a code that they can work by.

***It has to their code ... it is about their Life ... it must be PERSONAL.***

A personal remote workflow is definitely NOT a one-size-fits all kind of thing ... but in order to get going, we need to start somewhere.  From a technology standpoint, our starting point is fully internalizing the ENTIRE Gitlab workflow ... and that includes contributing to the imporovement of Gitlab for other people who use Gitlab. At the risk of sounding like a Gitlab commercial -- it's best to just adopt the entire set of embedded habits, to make Gitlab workflow your personal workflow discipline and where it doesn't work, improve it ... because it's all open source.

**Get more sleep; get better sleep** -- Practice good sleep hygiene. Work at rebuilding your circadian rhythms to better conform to the local sunrise/sunset. 

**Get going EARLY every day of the week** -- This is not just about discipline, but that, too. For optimal health and synching with the planet, it is probably best to start and end with a celebration of the workday at [astronomic twilight](https://en.wikipedia.org/wiki/Twilight#Astronomical_twilight) ... when when the geometric center of the Sun is 18 degrees below the horizon. Ideally, remote work affords one the opportunity to [at least occassionally] live or camp in a place where the sky is dark enough for nearly all astronomical observations, astronomers can easily make observations of point sources such as stars both during and after astronomical twilight in the evening and both before and during astronomical twilight in the morning. 

**TABATAs of exercise** -- Remote workers should start their day off with exercise and then consistently exercise much more throughout the day. Obviously, the same physical energy applied to exercise can be applied to a minor household chore or landscaping activity.  The important thing is AVOID SITTING, especially for prolonged periods ... to pace and think, do physical activities [like lawn mowing or snow shovelling] and think and to generally get one's blood flowing in order to do knowledge work. 

**30 minute time slots** -- Remote workers should strongly consider breaking their days into 30 minute time chunks, The 30 minutes time windows can include a 25-minute Pomodoro of intense focus on one item, a 4-minute TABATA of exercise/chores, a 1-minute refill/regroup/re-focus/re-boot [OODA loop](https://en.wikipedia.org/wiki/OODA_loop).

**Do not EVER just sit** -- As a reminder, semove ALL of your chairs. Stand. Pace. Try work with a mobile device by going for a walk. Use wrestling mats; roll on the floor.  If you really must to sit in order to focus or perhaps to get variety or a change of posture, get a stability ball or three. Do not ever sit in a chair that supports your passive, lazy, dead slug of a body ... your skeleton and muscles will not atrophy if they bear your weight or use your weight for strengthening and toning your body all day long.  
