---
title: Quality Discipline
subtitle: The Pareto Principle
date: 2021-03-14
tags: ["whiners", "Pareto"]

---

Spend 70% of your time on the vital few 10% of the key issues driving 85-90% of the headaches. Spend 25% of your time on the next 10%, second biggest drivers that drive most of the rest of the headaches. Spend 5% your time in surveillance, looking for the NEXT thing that might become part of the vital few.

In rapid order the fire-fighting will be over and most of your time will be spent in surveillance.

The DISCIPLINE of the pareto principle involves refusing to pay attention to whiners and attention whores ... people who have actually being in thick of fighting different kinds of fires NEVER EVER WHINE or seek attention ... but, because they understand why whiners and attention whores are so dangerous to the greater good, they WILL go after the whiners and attention whores when hysteria-for-hysteria's sake is driving 85-90% of the headaches.