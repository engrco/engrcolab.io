---
title:  The ENGRco process is about LEADING with YOUR LIFE.
subtitle: Containers, service meshes, microservices, immutable infrastructure, and declarative APIs 
date: 2021-04-14
tags: ["technology", "leadership"]
---

**Priorities!!!** *we'll get to the tech jargon bingo soon enough.*

# LEAD an antifragile life FIRST

*Lead with your LIFE.* To **BE** a leader ... *YOU must LEAD with your intensely PERSONAL example of being able to improve your health, to better ENJOY the gift of being ALIVE.*  

The ENGRco process is about LEADING with YOUR LIFE.

[Dogfooding](https://en.wikipedia.org/wiki/Eating_your_own_dog_food) the technological tools for better listening defines ENGRco. As a team, we consume our own product first ... we thoroughly *USE* these five technologies to help us get better at LISTENING. Dogfooding is a constant cycle of learning, using, developing, extending, evolving the technologies that one uses to live and even be able to learn, use, develop, extend and evolve the technology. 

# We become that which we consume

Our identities influence and are influenced by what we consume ... our identities shape how we listen, how we learn by developing and who joins our team to help us listen and learn.  ***The ENGRco process is fundamentally about Life!***


## Machine Learning Operations and Data Engineering

The area that we focus upon is machine learning and machine learning operations.  AI/ML Ops depends upon data wrangling, consuming data APIs from a wide range of sources while also generating data for GraphQL data APIs. There are going to be a lot of different approaches that have been and will continue to be used in structuring APIs ... there's not really an substitute for being conversant in how people think about [imperative, declarative, and workflow APIs](https://apievangelist.com/2019/07/16/imperative-declarative-and-workflow-apis/) ... although you probably want to start getting yourself and others to move in the direction of primarily [thinking in graphs](https://graphql.org/learn/thinking-in-graphs/) because graphs are especially powerful tools for humans who think visually so modeling many real-world phenomena tends to generally resemble our natural mental models and verbal descriptions of underlying processes. 

## Immutable Infrastructure

Under the *immutable infrastructure* paradigm, servers are never modified after they are deployed. If something needs to be updated, fixed, or modified in any way, new servers built from a common image with the appropriate changes are provisioned to replace the old ones. After they’re validated, the new servers are put into use and the old ones are decommissioned ... this might sound like a big ordeal to sysadmins from the 2010s or 2000s, except that NOW these processes are now entirely automated and scripted, so that when code is actually ready for production, the process, if done right, is supposed to be damned FAST, ie minutes, in not seconds.

Under this *immutable infrastructure* paradigm ALL of the test, development, integration, deployment infrastructure is also immutable ... and the scripts/automation are continually improved/updated to make the automated process more effective at dilivering much more repeatable, more reliable, more scaleable systems FASTER, monitored and analyzed to make the process still FASTER. Using [GitOps](https://learning.oreilly.com/library/view/repeatability-reliability-scalability/9781801077798/) for version-controlled code in CI/CD pipelines according to the best of the best practices in [Infrastructure-as-Code](https://learning.oreilly.com/topics/infrastructure-as-code/) is about really *using* proven software development frameworks to break systems into smaller pieces and then use test-driven development and bake in observability into continously integrated and deployed code ... code which configures / builds infrastructure consistently and predictably.  

The point of SPEED is all about increasingly the frequency to get smaller, less noticeable, less likely to fail, **safer** changes in fault-tolerant systems ... it's really about pushing for **better and more secure end-user experiences**.

## Microservices 

Distributed systems have become more fine-grained as organizations shift from code-heavy monolithic applications to smaller, self-contained [microservices](https://learning.oreilly.com/playlists/b04ccb0d-5c32-4dcb-87f3-ed4022a053d4/). Of course the *evolutionary* paradigm of this *evolving* architecture is also *evolving*. This kind of continual improvement toward even mroe resilient, more manageable, more observable requires loosely coupled systems and more genuinely agile programming teams ... frankly, most programmers are not going to be capable of developing this level of team agility. The evolving architecture of microservices demands team agility and this makes life exciting and generally keeps things interesting ... but for those who are not immersed in this realm every day, it can be a challenge to see where the forest is going while in the midst of rapidly morphing trees while [building, managing, and evolving microservices architectures](https://learning.oreilly.com/library/view/building-microservices-2nd/9781492034018/).

## Service Meshes

*Your priority in life should be upon LEADING with your example ... put yourself out there and then LISTEN to the reactions.*  

As organizations adopts microservices, they need [better service mesh patterns](https://learning.oreilly.com/library/view/service-mesh-patterns/9781492086444/) for that  infrastructure layer which handles interservice communication in microservice architectures. 

## Containers

*Your priority in life should be upon LEADING with your example ... put yourself out there and then LISTEN to the reactions.*

Always be reading ... you probably cannot know enough about [containers](https://learning.oreilly.com/playlists/fdde36b9-4103-484e-afee-85efac6d91c3/) and the use of [Kupernetes](https://learning.oreilly.com/topics/kubernetes/) to deploy container-based distributed applications.