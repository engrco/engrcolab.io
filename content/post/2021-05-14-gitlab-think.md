---
title:  Contributing to GitLab OpenSource
subtitle: Our attempt to get GitLab to think like we think ... or the other way around ... but does it matter?
date: 2021-05-14
tags: ["technology", "leadership"]
---

We intend to so thoroughly ***USE*** the whole GitLab process, which includes [contributing to Gitlab](https://about.gitlab.com/community/contribute/) ... so THOROUGHLY that GitLab starts to think like us.

Well, maybe it'll be the other way around. Most of all, it just won't matter who thinks like who.  Or, maybe that's the whole point of really *using* and [contributing](https://about.gitlab.com/community/contribute/) open source development pipeline SaaS.

Of course, we have a lot to learn ... well, yeah, that's pretty obvious, right?  *Doesn't EVERYONE who is actually alive have a lot to learn?*  Contributing to open source is fundamentally about really getting at the foundation of ***learning*** how to think ... we have a very, very, very LONG professional history of LEAN thinking and that includes the impact that LEAN has on safety, reliablity, quality and SPEED ... regardless of what automation fans think about automation speeding things up, you cannot possibly go fast without LEAN thinking ... without LEAN, you automate the production CRAP ... and CRAP inevitably finds ways to not only slow you down, but kill the whole show.

So we intend to get GitLab to think like us ... LEAN thinking applied to test-driven development (TDD) has been around basically forever in serious quality assurance space, ie way before trendy bs-speak of *Shift-Left* or *DevSecOps.* 

LEAN TDD has consistenly always provided the best results ... there are plenty of more eloquent writers writing on this topic ... The Phoenix Project, Accelerate, and The DevOps Handbook. If you work in the realm of developing anything and have not read and internalized the learnings in these books, you SHOULD NOT be successful in any organization that produces anything ... but that does not mean that everyone gets it ... companies still fail because people have not yet internalized the essentials ... [Alex Kreilein](https://alexkreilein.medium.com/) has done a MASTERFUL job in efficiently summarizing this material; you will definitely want to FOLLOW his writing.

A few key outcomes of LEAN TDD might illustrate why you should read Kreilein's thoughts on [Smashing the Shift Left Unicorn](https://alexkreilein.medium.com/smashing-the-shift-left-unicorn-524e36783a64):

1) **Find bugs and WASTE early** ... so early that the bad thinking behind the original intent is stopped cold and maybe fired if that bad thinking refuses to learn and doesn't knock it off

2) **Tighter designed, cleaner, more extensible code** ... and, when it's not, we go back and improve the test cases and then ... really bear down and get dead serious about improving the underlying TDD and requirements engineering processes which are producing the less than optimal test cases.

3) **Code that just works** ... the code actually meets hard info sec and functional requirements first, satisfies customer acceptance criteria [per contract, or dev doesn't get paid] for even *squishy* wants second and, then might even tackle the *softer* articulated nice-to-have expectations of end users ... because those things are what the test cases are and test-cases are prioritized or ranked.

4) **Better code refactoring processes** ... high quality, LEAN code makes it possible to more clearly see how/where refactoring should happen

5) **Happy developers, happy teams, financial success** ... because stupid, recalcitrantly bad non-TDD thinking cannot cope with TDD ... and then either find a new line of work OR go to work for a competitor who doesn't enforce TDD


