---
title:  ML Operations
subtitle: What about IoT, what about the Edges
date: 2021-06-14
tags: ["technology", "leadership"]
---

We can use containers to move ML ops out to the edge devices, eg phones / chromebooks ... and beyond that out to IoT, eg Raspberry Pi, smart sensors.

Do this and overcoming the constraints of doing this might be an interesting enough puzzle in an esoteric sense, but ... *WHY?*

WHY is ALWAYS one of the really important questions that we need to think about is WHY would we even want to bother?  

## WHY?

It's the classic "Know You Why" thing that has to dominate your project management thinking ... you HAVE TO MANAGE SCOPE ... because there's no end to the interesting things you can noodle around with.  At first a project can be a small labor of love, a side-project, some hobby thing or a goofy experimental toy that someone is tinkering with just to explore/test capabilities or in order to learn how something might work ... but really quickly in the larger lifecycle, the hobby project has to be scoped [perhaps informally, at first] in order to focus upon completing some sort of [barely] working toy that others can play with.

As soon as we have something that people can play with ... we will need to have a reason for why this is needed ... unless it's just always going to be something that is only a [soon-to-be-annoying] "flash in the pan," ie, like the blinking neon text on a webpage in the 1990s.  Curiousity can be enough, at first -- but before long, there needs to be serious thought given to "WHY?" 

Anyone can be as curious as they want to be about their hobby project -- but serious thought about the WHY is necessary to convince others to care ... and other capable people will have no shortage of interesting things to explore, so the NEED has to be real, has to be articulated for attracting others.

## How huge is the NEED for this problem? 

What's the severity? How many humans are suffering or dying? So what if this remains unsolved?

## Who is going to pay or how does this get supported or monetized? Who parts with their time or $ in order to advance the cause?

... BUT at a certain point ... this neat little prototypey thingey will need to be supported or monetized in some way to ensure that people who work on it can at least pay their utility/food bills ... so that issues are dealt with expeditiously, professionally.

## Timing. Ecosystems of enabling technologies. Wider spread recoginition of the need and how this addresses the need.
