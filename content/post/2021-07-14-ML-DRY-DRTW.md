---
title:  Don’t Reinvent The Wheel (DRTW)
subtitle: The more important corollary of DRY
date: 2021-07-14
tags: ["technology", "focus"]
---

The Don’t Repeat Yourself (DRY) principle religiously in programming. Programs should be built of programs and code that works -- if something NEEDS be done better, it should be re-factored to be simpler and more robust. However, just implementing something in a different way is ... almost offensive. Don’t Reinvent The Wheel (DRTW) is a close corollary to DRY. Reimplementing something when an existing implementation is more than good enough is a worse than an unnecessary waste of time -- if the second approach is somehow successful, the attention devoted to solving the problem is fork and the second approach weakens the first. 

***There are enough things that NEED to be done ... we should devote more time to THINKING about the NEED and less time to just doing something to prove that we can do something.***

[Ruby on Rails](https://guides.rubyonrails.org/) is not really JUST an application framework -- instead, Rails is a vast ecosystem of core code, official plugins, and third-party plugins, [AWESOME things related to Rails](https://github.com/gramantin/awesome-rails), [developers blogging about the future of Rails](https://blog.railwaymen.org/10-ruby-on-rails-blogs-you-should-be-following) ... and at current count, [5871 contributors](https://contributors.rubyonrails.org/) ... except, of course, that count does not take into consideration all of the people who've answered [a quarter of a million questions on StackOverflow](https://stackoverflow.com/questions/tagged/ruby-on-rails?sort=MostVotes&edited=true) or have used [Rails applications like Github](https://github.com/topics/rails) to build *open source* applications [like Gitlab] which are changing the way that developers build things ... a serious Don’t Reinvent The Wheel (DRTW) mindset means that in 2021, the wheel is pretty darn good -- you should spend a lot more time looking through other people's code ... forking repositories and playing with the code in order to learn or understand better is okay, but the preferred way to improve upon project is to contribute to that project.

Don’t Reinvent The Wheel (DRTW) is not about just settling for a crappy wheel -- it's about focusing on understanding the existing solutions and, when you have an idea for an improved wheel, it's about joining and contriubuting to a community, ie proposing a fix and submitting your idea for comments/debates and generally make the overally community smarter and better at thinking RATHER than just forking a repo and hacking a fix.

## Remove The Automation That's Obscuring The Barnacles on The Wheel

As with most other forms of improvement, most of the gains come from REMOVING the impediments, and that includes automation, pain relievers and medicines ... most of improvement is in knowing what to take away, but generally it's better to err on the side of removal ... do not just remove ALL of the noise, remove all of the clever automation that is amping up the noise into something that seems to be important because it's loud fuzz.

When you LISTEN carefully to ideas, you will find that MOST of what you have been listening to is just DEAD manipulation, not actually an idea.
