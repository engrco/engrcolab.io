---
title:  Read BETTER
subtitle: Much More With Better Selection Skills, Not Just Faster But Better Comprehension
date: 2021-08-12
tags: ["technology", "leadership"]
---

Reading is one of those complex skill stacks that one must continually improve [if one want to be useful to others] ... continually ***improving*** in reading, or the intake of information, is a fundamental and necessary engineering skill.

Reading is not exactly about visually reading the printed word ... it's more general ... it is about screening, selecting, filtering, determining context to efficiently cut to the chase to take in in significantly more information.  Reading is about the awake FOCUSED, efficient intake of firehoses of information.

I don't recommend ever watching any crap on the teevee ... because the data are good enough and [this example of an app pulling from a data API](https://www.espn.com/college-football/playbyplay/_/gameId/401309872?) illustrates why ... take a quick peek at these data and you will SEE what happened at the end of that Oregon game.

Playing AT Oregon ... AT Oregon, in front of the 12th man in Eugene ... the completely hapless 1-5 Cal-Berkeley team almost won on the final play of the game -- the Bears had the ball on Oregon's 2 yard line, practically a TD -- but couldn't quite punch'r in and do the 2-pt converv to BEAT the most pathetic excuse for a Top 10 team ever ... it's not remotely like how Alabama or Clemson fell apart ... in 100X less time than it took you to READ this ... you didn't have to waste time turning on the tube ... even that would be 100X better than traveling to Eugene, giving your virulent strain of covid to the entire West Coast and watching the Ducks get totally BEAT [except for a final score] ... it was not just actually getting outscored by a mediocre Stanford, but even worse -- the Ducks effectively tied with the 1-5 Bears ... look at the data.

**The POINT** is that IF you want to read orders of magnitude more, better, faster and especially IF you want to have time to level up your antiquated skillset to be able to develop and train intelligent machine models to use your own AI to sort, sift, recommend better material for you ... THEN you need to stop wasting time watching the teevee, imagining that video or sportscasting is going to offer a better oppurtunity to take in information than READING [and machine-augmented reading].  
## With some exceptions, AVOID video

Focus and awakeness is not really possible with most video, since most video involves far too many distractions and eye-catching pieces of nonsense, eg you might want to see a pic of the guest, but MOSTLY the visuals are about programming you to relax, be comfortable and enjoy the show as you nod off ... for example, the large neon sign behind Joe Rogan on the JRE is JUST a distraction or a time-honored symbol of home-sweet-home man-cave-ness which puts Joe's audience at ease, in the same way that Charlie Rose's big dining/conference table with the soft lighting put Charlie's aging liberal pseudo-intellectual audience at ease ... or the way that Steve Allen's [or Johnny Carson's, Dave Letterman's, Jay Leno's couch with piano and/or jazz band] on the prototypical late night talk show relaxed the audience and let them unwind for bed [or staying up late like the big boys do]. 

It's perfectly fine if you want to RELAX with video -- don't imagine that it is making you more informed in such a passive experience ... ***video LEADS and PROGRAMS you*** ... it tells a STORY ... understand the difference between a *watching a story* and *gathering intelligence* ... *be aware and in control of your programming.*