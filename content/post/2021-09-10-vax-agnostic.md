---
title:  Big Pharma Agnostic
subtitle: What have you done for me ... EVER?
date: 2021-09-10
tags: ["technology", "leadership"]
---

I am a Big Pharma / Big Medicine / Big Sciene agnostic ... I WANT TO be a believer, but I just can't. 

It's not exacty like I don't see the gains. Anyone can read read the peer-reviewed journals, the pre-print articles and other tech pub papers ... it's impressive when one considers things like the increases in analytic firepower of gene sequences, transciptomics technology or molecular biology ... it's hard not to be impressed, EXCEPT... 

Explain why we are still losing the War on Cancer, why productive working healthspan is going down, why people legitimately FEAR a medically-induced financial catastrophe, or more recently, WHY are people SO INCREDIBLY AFRAID of a virus that mostly only attacks those who already have compromised immune systems. 

WHY isn't Big Medicine CURING things and putting itself out of business by giving people even better options. Why isn't health-care about HEALTH and generally making people functionally healthier and productive ... why are people continually LESS in need of heroic life-saving miraculous cures?  

WHY do we need the the religous faith in Big Pharma's miracles ... why do we need something a MANDATE to get people to be vaccinated???

***Seriously ... WHY?***   

A big part of the why is that people don't know the meaning or significance of LIFE ... people completely don't know at all how to deal with the inevitability of death or why every second of productive, HEALTHY life is so precious.

It is the unquestioning faith of others in something that provides little if any apparent benefit that makes me so nervous ... since I am not particular a fan of any dependency upon drugs ... especially tightly-rationed prescription drugs or vaccinations that one can only obtain from the ordained high priests of Big Medicine religion.

I don't mean to sound so ungrateful ... I won't deny all benefits of drugs or vaccinations ... I also am troubled by the zealotry of the believers in the anti-vax cause ... I tend to get nervous quick when people pain something with either black or white -- my world is mostly full of grey ... and more than a few colors.

I do find that as I get older, I do occasionally find that I a couple aspirin are helpful every so often -- not something one uses every day or even every week. My use of aspirin is usaally tied to old me violating some limit that young me didn't think applied to me ... I use TagaMet, much less than aspirin but for a similar reason, when I don't obey my own rules and avoid the sugar/carbs/pizza/bread that I should. I don't use Ivermectin, but I can understand why something like Iveemectin might help certain people in certain situations ... and, in general, that applies to the entire universe of pharmaceuticals for me ... I think that sustained daily use over a long term [not tempoarily, like taking painkillers while recovering from surgery] is pretty much proof of someone not having any desire to get to the root cause of why the drug was ever prescribed. 

Mostly, I avoid pills/potions ... "*MOSTLY*" means that occasionally I break the rules ... I am an extremely irregular [almost never any more] enjoyer of cigars and smoking pipes, ie nicotine is most enjoyable when one's lungs are healthy [because one never smokes] ... I am a completely unapologetic caffiene addict, even though I know that it destroys my sleep hygiene, ie I understand why people quit; I am not ready to quit caffeine YET although I expect that someday the more spectacular view that caffiene has offered might not be worth the climb any more.

The MANIACAL push for people to be vaccinated against covid tells me that I am an agnostic living in a land of fundamentalist zealot extremists who NEED to require that all non-believers are *liquidated* ... ESPECIALLY because of the fact that the non-believers might have a better chance of survival.

***FREEDOM of think is dangerous to the zealots.***