---
title:  Write ... Blog
subtitle: Structure Provides the Framework For Comprehension and Conveying Historical Significance
date: 2021-10-02
tags: ["technology", "leadership"]
---

Writing is one of those complex skill stacks that one must continually improve [if one want to be useful to others] ... continually ***improving*** in writing, specifically in the output of necessary technical information, is a fundamental and necessary engineering skill. Engineers don't necessary write materials like Shakespeare or Dostovesky, but they write things like instructions, troubleshooting tools or technical documents which are indispensable to those who use, install or implement of techonology that people depend upon.

Writing is not exactly about writing the visually, useful bullet-pointed instructions ... but generally white space matters ... it's the same with speaking ... it is about EMPHASIS ... driving FOCUS in the reader, who does not care at all about the text ...until the reader gets the bullet points.

## Structure really matters. 

Most people are teevee watchers ... they get their ideas spoon fed to them by journalists who write with a pathetically similar structure ... but maybe that structure is what matters most.

**The POINT** is that IF you want to Write orders of magnitude more, better, faster and especially IF you want to have time to level up your antiquated skillset to be able to develop and train intelligent machine models to use your own AI to sort, sift, recommend better material for you ... THEN you need to stop wasting time watching the teevee, imagining that video or sportscasting is going to offer a better oppurtunity to take in information than Writing [and machine-augmented Writing].  

## Write like a historian