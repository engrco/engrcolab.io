---
title:  Speak, Podcast, Produce
subtitle: Toastmasters to Podcast Production
date: 2021-10-10
tags: ["technology", "leadership"]
---



In my experience, people either understand why they personally need to be proficient at public speaking OR there is no possible to way to convince them of that need ... you get it or you don't. I believe that the biggest reason is self-awareness, self-confidence and proficiency as well as all of the opportunities to meet other intelligence people who are proficient and self-confident enough to be humble and self-aware. You either get this or you are not going to right now, but someday you will.

If you want to be better at public speaking or presenting at conferences, you need to speak in front of lots of different people, ideally some of that happens in front of knowledgable discrimating audiences but the other challenging audience is an audience that does not really know the topic or care all that much about it.

You will never master public speaking without doing it really, really, REALLY often ... nowadays, the capabilities of affordable audio equipment are amazing and the inexpensive digital audio workstations are even more amazing, you can produce audio faster, more efficiently, more inexpensively than you can ever begin to travel to lots of speaking venues or conferences ... so your need to master public speaking might mean that you should consider producing a podcast.

## Find Your Voice

The first thing that you might feel like to you want to do is to find your voice ... FORGET THAT.  

You will find your voice by finding your voice ... the first try might be perfect ... but in all likelihood, your best idea, your best development of content, your best production values will be pretty okayish but, frankly, they will just not be good enough ... and you will be discouraged ... because you grossly underestimated how insanely tough it is to make something look like anybody could do it.  

The price of gear and the learning curve for all of the production engineering is just not an obstacle any more ... anybody could do it ... except that almost nobody does ... because nobody really enjoys failing that much, that often, that regularly.  

The secret that nobody want to admit is that even the best of the best of the very best ... FAIL ... and fail every single time out.  You might not recognize it, but it's the same in any kind of professional activity ... once you get damned good, recognized, certified, appreciated, paid well -- you will see stuff that you are doing that just is not quite yet good enough and the inadequacies will gnaw at you, keep you up at nights, give you stress and aggravation. Aggravation is what being a professional is all about.

ONLY the grossly incompetent, completely clueless LOSERS in the world ever blame someone else for their lack of achievement. *Winners NEVER blame the refs, NEVER blame somebody for cheating, NEVER EVER blame their teammates or coaches.* ***You can identify a CERTIFIED LOSER by looking at how they blame others rather than talking about what they need to work on.***